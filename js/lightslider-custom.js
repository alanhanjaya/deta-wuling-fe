const prevIcon = '<i class="fa-solid fa-chevron-left sales-arrow"></i>';
const nextIcon = '<i class="fa-solid fa-chevron-right sales-arrow"></i>';
const prevIcon1 = '<i class="fa-solid fa-chevron-left desain-arrow"></i>';
const nextIcon1 = '<i class="fa-solid fa-chevron-right desain-arrow"></i>';

$(document).ready(function() {
    $("#sales").lightSlider({
        onSliderLoad: function (el) {
            var maxHeight = 0,
              container = $(el),
              children = container.children();
            children.each(function () {
              var childHeight = $(this).height();
              if (childHeight > maxHeight) {
                maxHeight = childHeight;
              }
            });
            container.height(maxHeight);
        },
        item:4,
        loop:true,
        keyPress:false,
        pager:false,
        responsive : [
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ],
        prevHtml : prevIcon,
        nextHtml : nextIcon
    });
});

$(document).ready(function() {
  $("#desain").lightSlider({
      onSliderLoad: function (el) {
          var maxHeight = 0,
            container = $(el),
            children = container.children();
          children.each(function () {
            var childHeight = $(this).height();
            if (childHeight > maxHeight) {
              maxHeight = childHeight;
            }
          });
          container.height(maxHeight);
      },
      item:3,
      loop:true,
      keyPress:false,
      pager:false,
      responsive : [
          {
              breakpoint:480,
              settings: {
                  item:1,
                  slideMove:1
                }
          }
      ],
      prevHtml : prevIcon1,
      nextHtml : nextIcon1
  });
});

$(document).ready(function() {
  $("#foto-slider").lightSlider({
      auto:true,
      pause:4000,
      loop:true,
      keyPress:false,
      pager:false,
      responsive : [
          {
              breakpoint:480,
              settings: {
                  item:1,
                  slideMove:1
                }
          }
      ]
  });
});
